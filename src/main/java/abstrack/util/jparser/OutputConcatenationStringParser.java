package abstrack.util.jparser;

import abstrack.util.jparser.and.Tuple;

final class OutputConcatenationStringParser implements Parser<String, String>
{
    private final Parser<? super String, ? extends Tuple<String, String>> parser;

    OutputConcatenationStringParser(
        final Parser<? super String, ? extends Tuple<String, String>> parser)
    {
        this.parser = parser;
    }

    @Override
    public String apply(final String input)
    {
        return this.parser.apply(input).flatMap(String::concat);
    }
}
