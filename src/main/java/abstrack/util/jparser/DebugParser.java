package abstrack.util.jparser;

import java.util.function.Consumer;

final class DebugParser<Input, Output> implements Parser<Input, Output>
{
    private final Consumer<? super String> consumer;
    private final String name;
    private final Parser<? super Input, ? extends Output> parser;

    DebugParser(
        final Consumer<? super String> consumer,
        final String name,
        final Parser<? super Input, ? extends Output> parser)
    {
        this.consumer = consumer;
        this.name = name;
        this.parser = parser;
    }

    @Override
    public Output apply(final Input input)
    {
        this.consumer.accept(String.format("'%s' parser is parsing:\n%s", this.name, input));
        final var result = this.parser.apply(input);
        this.consumer.accept(String.format("'%s' parser finished.", this.name));
        return result;
    }
}
