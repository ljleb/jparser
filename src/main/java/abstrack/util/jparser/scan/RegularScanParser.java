package abstrack.util.jparser.scan;

import abstrack.util.jparser.Parser;

final class RegularScanParser<Input> implements ScanParser<Input>
{
    private final Parser<? super Input, ?> parser;

    RegularScanParser(final Parser<? super Input, ?> parser)
    {
        this.parser = parser;
    }

    @Override
    public Void apply(final Input input)
    {
        this.parser.apply(input);
        return null;
    }
}
