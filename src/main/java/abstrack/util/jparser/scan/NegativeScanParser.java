package abstrack.util.jparser.scan;

import abstrack.util.jparser.SyntaxException;

final class NegativeScanParser<Input> implements ScanParser<Input>
{
    private final ScanParser<? super Input> parser;

    NegativeScanParser(final ScanParser<? super Input> parser)
    {
        this.parser = parser;
    }

    @Override
    public Void apply(final Input input)
    {
        try
        {
            this.parser.apply(input);
        }
        catch(final SyntaxException e)
        {
            return null;
        }
        throw new NegativeScanException(input);
    }
}
