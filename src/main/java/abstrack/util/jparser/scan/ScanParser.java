package abstrack.util.jparser.scan;

import abstrack.util.jparser.Parser;

@FunctionalInterface
public interface ScanParser<Input> extends Parser<Input, Void>
{
    static <Input> ScanParser<Input> of(final Parser<? super Input, ?> parser)
    {
        return new RegularScanParser<>(parser);
    }

    default ScanParser<Input> negative()
    {
        return new NegativeScanParser<>(this);
    }
}
