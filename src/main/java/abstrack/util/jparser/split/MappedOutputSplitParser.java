package abstrack.util.jparser.split;

import abstrack.util.jparser.and.Tuple;

import java.util.function.Function;

final class MappedOutputSplitParser<Input, Output, NewOutput> implements SplitParser<Input, NewOutput>
{
    private final Function<? super Input, ? extends Tuple<Input, Output>> splitFunction;
    private final Function<? super Output, ? extends NewOutput> outputFunction;

    MappedOutputSplitParser(
        final Function<? super Input, ? extends Tuple<Input, Output>> splitFunction,
        final Function<? super Output, ? extends NewOutput> outputFunction)
    {
        this.splitFunction = splitFunction;
        this.outputFunction = outputFunction;
    }

    @Override
    public Tuple<Input, NewOutput> apply(final Input input)
    {
        final var result = this.splitFunction.apply(input);
        return result.mapRight(this.outputFunction);
    }
}
