package abstrack.util.jparser.split;

import abstrack.util.jparser.and.Tuple;

import java.util.function.Function;

final class RegularSplitParser<Input, Output> implements SplitParser<Input, Output>
{
    private final Function<? super Input, ? extends Tuple<Input, Output>> andFunction;

    RegularSplitParser(final Function<? super Input, ? extends Tuple<Input, Output>> andFunction)
    {
        this.andFunction = andFunction;
    }

    @Override
    public Tuple<Input, Output> apply(final Input input)
    {
        return this.andFunction.apply(input);
    }
}
