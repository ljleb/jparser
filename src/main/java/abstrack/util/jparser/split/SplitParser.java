package abstrack.util.jparser.split;

import abstrack.util.jparser.and.AndParser;
import abstrack.util.jparser.and.Tuple;
import abstrack.util.jparser.many.ManyParser;

import java.util.function.Function;

@FunctionalInterface
public interface SplitParser<Input, Output> extends AndParser<Input, Input, Output>
{
    static <Input, Output> SplitParser<Input, Output> of(
        final Function<? super Input, ? extends Tuple<Input, Output>> andFunction)
    {
        return new RegularSplitParser<>(andFunction);
    }

    default SplitParser<Input, Output> chain(
        final Function<? super Input, ? extends Tuple<Input, Output>> splitFunction)
    {
        return input -> this.apply(input).flatMapLeft(splitFunction);
    }

    default ManyParser<Input, Output> many()
    {
        return ManyParser.of(this);
    }

    default <NewOutput> SplitParser<Input, NewOutput> mapOutput(
        final Function<? super Output, ? extends NewOutput> function)
    {
        return new MappedOutputSplitParser<>(this, function);
    }
}
