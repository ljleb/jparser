package abstrack.util.jparser;

import abstrack.util.jparser.and.Tuple;
import abstrack.util.jparser.split.SplitParser;

import java.util.regex.Pattern;

public final class RegexParser implements SplitParser<String, String>
{
    private final Pattern pattern;

    RegexParser(final String regex)
    {
        this.pattern = RegexParser.compile(regex);
    }

    private static Pattern compile(final String regex)
    {
        return Pattern.compile(String.format("^(%s)", regex), Pattern.DOTALL);
    }

    @Override
    public Tuple<String, String> apply(final String input)
    {
        return RegexParser.updateResult(input, this.tryMatch(input));
    }

    private String tryMatch(final String input)
    {
        final var matcher = this.pattern.matcher(input);
        if(!matcher.find())
        {
            throw new RegexMismatchSyntaxException(this.pattern, input);
        }
        return matcher.group();
    }

    private static Tuple<String, String> updateResult(
        final String input,
        final String match)
    {
        final var resultInput = input.replaceFirst(Pattern.quote(match), "");
        return Tuple.of(resultInput, match);
    }
}
