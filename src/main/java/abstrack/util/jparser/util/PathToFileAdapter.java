package abstrack.util.jparser.util;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

public final class PathToFileAdapter implements File
{
    private final Path path;

    PathToFileAdapter(final Path path)
    {
        this.path = path;
    }

    @Override
    public String getName()
    {
        return this.path.getFileName().toString();
    }

    @Override
    public String getContent()
    {
        try
        {
            return Files.readString(this.path);
        }
        catch(final IOException e)
        {
            throw new UncheckedIOException(e);
        }
    }
}
