package abstrack.util.jparser.util;

import java.nio.file.Path;

public interface File extends FileSystemObject<String>
{
    static File ofPath(final Path path)
    {
        return new PathToFileAdapter(path);
    }
}
