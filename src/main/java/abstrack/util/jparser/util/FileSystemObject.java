package abstrack.util.jparser.util;

public interface FileSystemObject<Content>
{
    String getName();

    Content getContent();
}
