package abstrack.util.jparser.and;

import java.util.function.Function;

final class RegularAndParser<Input, LeftOutput, RightOutput> implements AndParser<Input, LeftOutput, RightOutput>
{
    private final Function<? super Input, ? extends LeftOutput> left;
    private final Function<? super Input, ? extends RightOutput> right;

    RegularAndParser(
        final Function<? super Input, ? extends LeftOutput> left,
        final Function<? super Input, ? extends RightOutput> right)
    {
        this.left = left;
        this.right = right;
    }

    @Override
    public Tuple<LeftOutput, RightOutput> apply(final Input input)
    {
        final var leftTuple = this.left.apply(input);
        final var rightTuple = this.right.apply(input);
        return Tuple.of(leftTuple, rightTuple);
    }
}
