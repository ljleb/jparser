package abstrack.util.jparser.and;

import abstrack.util.jparser.Parser;

import java.util.function.BiFunction;
import java.util.function.Function;

public interface AndParser<Input, LeftOutput, RightOutput> extends Parser<Input, Tuple<LeftOutput, RightOutput>>
{
    static <Input, LeftOutput, RightOutput> AndParser<Input, LeftOutput, RightOutput> of(
        final Function<? super Input, ? extends LeftOutput> leftParser,
        final Function<? super Input, ? extends RightOutput> rightParser)
    {
        return new RegularAndParser<>(leftParser, rightParser);
    }

    default <NewOutput> Parser<Input, NewOutput> map(
        final BiFunction<? super LeftOutput, ? super RightOutput, ? extends NewOutput> biFunction)
    {
        return this.map(tuple -> tuple.flatMap(biFunction));
    }

    default <NewLeftOutput> AndParser<Input, NewLeftOutput, RightOutput> mapLeft(
        final Function<? super LeftOutput, ? extends NewLeftOutput> leftFunction)
    {
        return new MappedLeftAndParser<>(this, leftFunction);
    }

    default <NewRightOutput> AndParser<Input, LeftOutput, NewRightOutput> mapRight(
        final Function<? super RightOutput, ? extends NewRightOutput> rightFunction)
    {
        return new MappedRightAndParser<>(this, rightFunction);
    }
}
