package abstrack.util.jparser.and;

import java.util.function.Function;

final class MappedLeftAndParser<Input, LeftOutput, RightOutput, NewLeftOutput>
    implements AndParser<Input, NewLeftOutput, RightOutput>
{
    private final AndParser<? super Input, ? extends LeftOutput, RightOutput> andParser;
    private final Function<? super LeftOutput, ? extends NewLeftOutput> leftFunction;

    MappedLeftAndParser(
        final AndParser<? super Input, ? extends LeftOutput, RightOutput> andParser,
        final Function<? super LeftOutput, ? extends NewLeftOutput> leftFunction)
    {
        this.andParser = andParser;
        this.leftFunction = leftFunction;
    }

    @Override
    public Tuple<NewLeftOutput, RightOutput> apply(final Input input)
    {
        final var result = this.andParser.apply(input);
        return result.mapLeft(this.leftFunction);
    }
}
