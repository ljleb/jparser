package abstrack.util.jparser.and;

import java.util.function.BiFunction;

public final class RegularTuple<Left, Right> implements Tuple<Left, Right>
{
    private final Left left;
    private final Right right;

    RegularTuple(final Left left, final Right right)
    {
        this.left = left;
        this.right = right;
    }

    @Override
    public <T> T flatMap(final BiFunction<? super Left, ? super Right, ? extends T> function)
    {
        return function.apply(this.left, this.right);
    }
}
