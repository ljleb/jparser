package abstrack.util.jparser.map;

import java.util.function.Function;

final class RegularMapParser<Input, Output, NewOutput> implements MapParser<Input, NewOutput>
{
    private final Function<? super Input, ? extends Output> beforeFunction;
    private final Function<?super Output, ? extends NewOutput> afterFunction;

    RegularMapParser(
        final Function<? super Input, ? extends Output> beforeFunction,
        final Function<?super Output, ? extends NewOutput> afterFunction)
    {
        this.beforeFunction = beforeFunction;
        this.afterFunction = afterFunction;
    }

    @Override
    public NewOutput apply(final Input input)
    {
        final var beforeOutput = this.beforeFunction.apply(input);
        return this.afterFunction.apply(beforeOutput);
    }
}
