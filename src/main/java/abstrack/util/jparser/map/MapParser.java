package abstrack.util.jparser.map;

import abstrack.util.jparser.Parser;

import java.util.function.Function;

@FunctionalInterface
public interface MapParser<Input, Output> extends Parser<Input, Output>
{
    static <Input, Output, NewOutput> MapParser<Input, NewOutput> of(
            final Function<? super Input, ? extends Output> identity,
            final Function<? super Output, ? extends  NewOutput> other)
    {
        return new RegularMapParser<>(identity, other);
    }
}
