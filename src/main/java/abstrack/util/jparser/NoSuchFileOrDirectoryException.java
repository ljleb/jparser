package abstrack.util.jparser;

import java.nio.file.Path;

class NoSuchFileOrDirectoryException extends SyntaxException
{
    NoSuchFileOrDirectoryException(final Path path)
    {
        super(String.format("no file or directory was found at '%s'", path.toUri()));
    }
}
