package abstrack.util.jparser;

import compiler.parser.util.File;
import compiler.parser.util.Folder;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Function;

public final class FileSystemParser<Output> implements Parser<Path, Output>
{
    public static <Output> Parser<Path, Output> of(
        final Function<? super File, ? extends Output> fileFunction,
        final Function<? super Folder, ? extends Output> folderFunction)
    {
        return new FileSystemParser<>(fileFunction, folderFunction);
    }

    private final Function<? super File, ? extends Output> fileFunction;
    private final Function<? super Folder, ? extends Output> folderFunction;

    private FileSystemParser(
        final Function<? super File, ? extends Output> fileFunction,
        final Function<? super Folder, ? extends Output> folderFunction)
    {
        this.fileFunction = fileFunction;
        this.folderFunction = folderFunction;
    }

    @Override
    public Output apply(final Path input)
    {
        if(Files.isRegularFile(input))
        {
            return this.fileFunction.apply(File.ofPath(input));
        }
        if(Files.isDirectory(input))
        {
            return this.folderFunction.apply(Folder.ofPath(input));
        }
        throw new NoSuchFileOrDirectoryException(input);
    }
}
