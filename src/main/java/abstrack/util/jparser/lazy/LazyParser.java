package abstrack.util.jparser.lazy;

import abstrack.util.jparser.Parser;

import java.util.function.Function;

@FunctionalInterface
public interface LazyParser<Input, Output> extends Parser<Input, Output>
{
    static <Input, Output> LazyParser<Input, Output> of(
            final Function<? super Input, ? extends Output> function)
    {
        return new RegularLazyParser<>(function);
    }

    @Override
    default LazyParser<Input, Output> lazy(final Function<? super Input, ? extends Output> function)
    {
        throw new ParserAlreadyLazyException();
    }
}
