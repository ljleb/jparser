package abstrack.util.jparser.lazy;

class ParserAlreadyLazyException
        extends RuntimeException
{
    ParserAlreadyLazyException()
    {
        super("cannot call Parser::lazy more than once");
    }
}
