package abstrack.util.jparser.lazy;

import java.util.function.Function;

final class RegularLazyParser<Input, Output> implements LazyParser<Input, Output>
{
    private final Function<? super Input, ? extends Output> function;

    RegularLazyParser(final Function<? super Input, ? extends Output> function)
    {
        this.function = function;
    }

    @Override
    public Output apply(final Input input)
    {
        return this.function.apply(input);
    }
}
