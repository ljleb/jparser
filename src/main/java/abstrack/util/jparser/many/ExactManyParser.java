package abstrack.util.jparser.many;

import abstrack.util.jparser.and.Tuple;
import com.google.common.collect.Iterables;

final class ExactManyParser<Input, Output> implements ManyParser<Input, Output>
{
    private final ManyParser<Input, Output> identity;
    private final int count;

    ExactManyParser(
        final ManyParser<Input, Output> identity,
        final int count)
    {
        this.identity = identity;
        this.count = count;
    }

    @Override
    public Tuple<Input, Iterable<Output>> apply(final Input input)
    {
        final var result = this.identity.apply(input);
        this.validateParseCount(Iterables.size(result.right()));
        return result;
    }

    private void validateParseCount(final int actualCount)
    {
        if(actualCount != this.count)
        {
            throw new IllegalParseCountSyntaxException(actualCount);
        }
    }
}
