package abstrack.util.jparser.many;

class IllegalParseCountSyntaxException extends RuntimeException
{
    IllegalParseCountSyntaxException(final int actualCount)
    {
        super(Integer.toString(actualCount));
    }
}
