package abstrack.util.jparser.many;

import abstrack.util.jparser.SyntaxException;
import abstrack.util.jparser.and.Tuple;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import java.util.function.Function;

public final class RegularManyParser<Input, Output> implements ManyParser<Input, Output>
{
    private final Function<? super Input, ? extends Tuple<Input, Output>> splitFunction;

    RegularManyParser(final Function<? super Input, ? extends Tuple<Input, Output>> splitFunction)
    {
        this.splitFunction = splitFunction;
    }

    @Override
    public Tuple<Input, Iterable<Output>> apply(final Input input)
    {
        return this.applyRecursive(Tuple.of(input, ImmutableList.of()));
    }

    private Tuple<Input, Iterable<Output>> applyRecursive(
        final Tuple<? extends Input, ? extends Iterable<Output>> partialOutput)
    {
        try
        {
            return this.applyRecursive(partialOutput
                .flatMap((input, output) -> this.splitFunction.apply(input)
                    .flatMap((newInput, element) -> Tuple.of(
                        newInput,
                        Iterables.concat(
                            output,
                            ImmutableList.of(element))))));
        }
        catch(final SyntaxException e)
        {
            return partialOutput.flatMap(Tuple::of);
        }
    }
}
