package abstrack.util.jparser.many;

import com.google.common.collect.Iterables;
import abstrack.util.jparser.and.Tuple;

final class MaximumManyParser<Input, Output> implements ManyParser<Input, Output>
{
    private final ManyParser<Input, Output> identity;
    private final int maximumCount;

    MaximumManyParser(
        final ManyParser<Input, Output> identity,
        final int maximumCount)
    {
        this.identity = identity;
        this.maximumCount = maximumCount;
    }

    @Override
    public Tuple<Input, Iterable<Output>> apply(final Input input)
    {
        final var result = this.identity.apply(input);
        this.validateParseCount(Iterables.size(result.right()));
        return result;
    }

    private void validateParseCount(final int actualCount)
    {
        if(actualCount > this.maximumCount)
        {
            throw new IllegalParseCountSyntaxException(actualCount);
        }
    }
}
