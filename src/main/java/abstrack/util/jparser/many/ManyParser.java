package abstrack.util.jparser.many;

import com.google.common.collect.Streams;
import abstrack.util.jparser.Parser;
import abstrack.util.jparser.and.Tuple;
import abstrack.util.jparser.optional.OptionalParser;

import java.util.function.BinaryOperator;
import java.util.function.Function;

@FunctionalInterface
public interface ManyParser<Input, Output> extends Parser<Input, Iterable<Output>>
{
    static <Input, Output> ManyParser<Input, Output> of(
        final Function<? super Input, ? extends Tuple<Input, Output>> splitParser)
    {
        return new RegularManyParser<>(splitParser);
    }

    default ManyParser<Input, Output> exactly(final int count)
    {
        return new ExactManyParser<>(this, count);
    }

    default ManyParser<Input, Output> min(final int minimumCount)
    {
        return new MinimumManyParser<>(this, minimumCount);
    }

    default ManyParser<Input, Output> max(final int maximumCount)
    {
        return new MaximumManyParser<>(this, maximumCount);
    }

    default OptionalParser<Input, Output> reduce(final BinaryOperator<Output> binaryOperator)
    {
        return this.map(outputs -> Streams.stream(outputs).reduce(binaryOperator))::apply;
    }
}
