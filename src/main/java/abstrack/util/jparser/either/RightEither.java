package abstrack.util.jparser.either;

import java.util.Optional;
import java.util.function.Function;

final class RightEither<Left, Right> implements Either<Left, Right>
{
    private final Right right;

    RightEither(final Right right)
    {
        this.right = right;
    }

    @Override
    public Optional<Left> left()
    {
        return Optional.empty();
    }

    @Override
    public Optional<Right> right()
    {
        return Optional.of(this.right);
    }

    @Override
    public <Output> Output map(
        final Function<? super Left, ? extends Output> leftFunction,
        final Function<? super Right, ? extends Output> rightFunction)
    {
        return rightFunction.apply(this.right);
    }
}
