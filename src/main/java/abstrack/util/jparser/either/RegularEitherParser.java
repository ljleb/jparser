package abstrack.util.jparser.either;

import abstrack.util.jparser.SyntaxException;

import java.util.function.Function;

final class RegularEitherParser<Input, LeftOutput, RightOutput> implements EitherParser<Input, LeftOutput, RightOutput>
{
    private final Function<? super Input, ? extends LeftOutput> leftFunction;
    private final Function<? super Input, ? extends RightOutput> rightFunction;

    RegularEitherParser(
        final Function<? super Input, ? extends LeftOutput> leftFunction,
        final Function<? super Input, ? extends RightOutput> rightFunction)
    {
        this.leftFunction = leftFunction;
        this.rightFunction = rightFunction;
    }

    @Override
    public Either<LeftOutput, RightOutput> apply(final Input input)
    {
        try
        {
            return Either.leftOf(this.leftFunction.apply(input));
        }
        catch(final SyntaxException e)
        {
            return Either.rightOf(this.rightFunction.apply(input));
        }
    }
}
