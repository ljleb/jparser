package abstrack.util.jparser;

import java.util.regex.Pattern;

class RegexMismatchSyntaxException extends SyntaxException
{
    RegexMismatchSyntaxException(final Pattern pattern, final String input)
    {
        super(String.format("could negative match %s in %s", pattern.pattern(), input));
    }
}
