package abstrack.util.jparser.optional;

import abstrack.util.jparser.SyntaxException;

import java.util.Optional;
import java.util.function.Function;

final class RegularOptionalParser<Input, Output> implements OptionalParser<Input, Output>
{
    private final Function<? super Input, ? extends Output> function;

    RegularOptionalParser(final Function<? super Input, ? extends Output> function)
    {
        this.function = function;
    }

    @Override
    public Optional<Output> apply(final Input input)
    {
        try
        {
            return Optional.of(this.function.apply(input));
        }
        catch(final SyntaxException error)
        {
            return Optional.empty();
        }
    }
}
