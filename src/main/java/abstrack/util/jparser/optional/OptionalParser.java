package abstrack.util.jparser.optional;

import abstrack.util.jparser.Parser;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

@FunctionalInterface
public interface OptionalParser<Input, Output> extends Parser<Input, Optional<Output>>
{
    static <Input, Output> OptionalParser<Input, Output> of(
        final Function<? super Input, ? extends Output> function)
    {
        return new RegularOptionalParser<>(function);
    }

    default Parser<Input, Output> orElse(final Output defaultValue)
    {
        return this.map(optional -> optional.orElse(defaultValue));
    }

    default Parser<Input, Output> orElseGet(final Supplier<? extends Output> defaultValueSupplier)
    {
        return this.map(optional -> optional.orElseGet(defaultValueSupplier));
    }
}
