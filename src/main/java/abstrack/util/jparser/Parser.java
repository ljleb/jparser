package abstrack.util.jparser;

import abstrack.util.jparser.and.AndParser;
import abstrack.util.jparser.either.EitherParser;
import abstrack.util.jparser.lazy.LazyParser;
import abstrack.util.jparser.map.MapParser;
import abstrack.util.jparser.scan.ScanParser;
import abstrack.util.jparser.optional.OptionalParser;

import java.util.function.Function;

@FunctionalInterface
public interface Parser<Input, Output> extends Function<Input, Output>
{
    default LazyParser<Input, Output> lazy()
    {
        return LazyParser.of(this);
    }

    @Override
    Output apply(Input input);

    default <NewOutput> Parser<Input, NewOutput> map(
        final Function<? super Output, ? extends NewOutput> function)
    {
        return MapParser.of(this, function);
    }

    default ScanParser<Input> scan()
    {
        return ScanParser.of(this);
    }

    default <RightOutput> AndParser<Input, Output, RightOutput> and(
        final Function<? super Input, ? extends RightOutput> other)
    {
        return AndParser.of(this, other);
    }

    default <RightOutput> EitherParser<Input, Output, RightOutput> or(
        final Function<? super Input, ? extends RightOutput> other)
    {
        return EitherParser.of(this, other);
    }

    default OptionalParser<Input, Output> optional()
    {
        return OptionalParser.of(this);
    }
}
